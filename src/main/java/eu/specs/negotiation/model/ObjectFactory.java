
package eu.specs.negotiation.model;

import eu.specs.negotiation.model.capability.Capability;
import eu.specs.negotiation.model.capability.ControlFramework;
import eu.specs.negotiation.model.capability.SecurityControl;
import eu.specs.negotiation.model.metrics.ConcreteMetric;
import eu.specs.negotiation.model.service.ServiceDescription;
import eu.specs.negotiation.model.service.ServiceResource;
import eu.specs.negotiation.model.slo.Expression;
import eu.specs.negotiation.model.slo.ObjectiveList;
import eu.specs.negotiation.model.slo.SLO;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.specs.negotiation.slom package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Endpoint_QNAME = new QName("http://specs-project.eu/schemas/sla_related", "endpoint");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.specs.negotiation.slom
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObjectiveList }
     * 
     */
    public ObjectiveList createObjectiveList() {
        return new ObjectiveList();
    }

    /**
     * Create an instance of {@link eu.specs.negotiation.model.service.ServiceDescription }
     * 
     */
    public ServiceDescription createServiceDescription() {
        return new ServiceDescription();
    }

    /**
     * Create an instance of {@link eu.specs.negotiation.model.capability.Capability }
     * 
     */
    public Capability createCapabilityType() {
        return new Capability();
    }

    /**
     * Create an instance of {@link ControlFramework }
     * 
     */
    public ControlFramework createCapabilityTypeControlFramework() {
        return new ControlFramework();
    }

    /**
     * Create an instance of {@link ServiceResource }
     * 
     */
    public ServiceResource createServiceDescriptionServiceResources() {
        return new ServiceResource();
    }

    /**
     * Create an instance of {@link ServiceResource.ResourcesProvider }
     * 
     */
    public ServiceResource.ResourcesProvider createServiceDescriptionServiceResourcesResourcesProvider() {
        return new ServiceResource.ResourcesProvider();
    }

    /**
     * Create an instance of {@link SLO }
     * 
     */
    public SLO createObjectiveListSLO() {
        return new SLO();
    }


    /**
     * Create an instance of {@link eu.specs.negotiation.model.metrics.ConcreteMetric }
     * 
     */
    public ConcreteMetric createServiceDescriptionSecurityMetrics() {
        return new ConcreteMetric();
    }

    /**
     * Create an instance of {@link SecurityControl }
     * 
     */
    public SecurityControl createCapabilityTypeControlFrameworkSecurityControl() {
        return new SecurityControl();
    }

    /**
     * Create an instance of {@link ServiceResource.ResourcesProvider.VM }
     * 
     */
    public ServiceResource.ResourcesProvider.VM createServiceDescriptionServiceResourcesResourcesProviderVM() {
        return new ServiceResource.ResourcesProvider.VM();
    }

    /**
     * Create an instance of {@link Expression }
     * 
     */
    public Expression createObjectiveListSLOExpression() {
        return new Expression();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://specs-project.eu/schemas/sla_related", name = "endpoint")
    public JAXBElement<String> createEndpoint(String value) {
        return new JAXBElement<String>(_Endpoint_QNAME, String.class, null, value);
    }

}
