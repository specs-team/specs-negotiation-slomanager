package eu.specs.negotiation.model;

import eu.specs.negotiation.communication.SLAPlatformClient;
import eu.specs.negotiation.model.metrics.AbstractMetric;
import eu.specs.negotiation.model.service.ServiceDescription;
import eu.specs.negotiation.model.slo.Expression;
import eu.specs.negotiation.model.slo.ObjectiveList;
import eu.specs.negotiation.model.slo.SLO;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by adrian on 3/31/15.
 */
@Path("/slo")
public class SLOAPI {

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public ObjectiveList getObjectiveList(List<String> names){
        ObjectiveList obl = new ObjectiveList();

        ServiceDescription sd = SLAPlatformClient.getStaticServiceDescription(names);

        for(AbstractMetric m : sd.getSecurityMetrics()){
            SLO slo = new SLO();
            slo.setName(m.getName());
            Expression e = new Expression();
            slo.setExpression(e);
            obl.getSLO().add(slo);
        }

        return obl;
    }
}
