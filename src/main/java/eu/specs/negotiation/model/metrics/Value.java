package eu.specs.negotiation.model.metrics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by adrian on 3/31/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetricDefinition", propOrder = {
        "value"
})
public class Value {

    @XmlElement(name = "value", required = true)
    protected String value;
}
