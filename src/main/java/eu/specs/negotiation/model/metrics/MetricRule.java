package eu.specs.negotiation.model.metrics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by adrian on 3/31/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetricDefinition", propOrder = {
        "definition", "note", "value"
})
public class MetricRule {


    @XmlElement(name = "value", required = true)
    private Value value;

    @XmlElement(name = "note", required = true)
    private String note;

    @XmlElement(name = "definition", required = true)
    private String definition;

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
}
