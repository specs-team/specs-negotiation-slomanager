package eu.specs.negotiation.model.metrics;

/**
 * Created by adrian on 3/31/15.
 */


public enum Qualitative {
    NOMINAL,
    ORDINAL,
}
