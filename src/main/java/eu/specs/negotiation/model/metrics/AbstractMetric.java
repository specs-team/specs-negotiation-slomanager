package eu.specs.negotiation.model.metrics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by adrian on 3/31/15.
 */

@XmlRootElement(name="metric")
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractMetric {
    @XmlElement(name = "name", required = true)
    protected String name;

    @XmlElement(required = true)
    protected MetricDefinition metricDefinition;

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public MetricDefinition getMetricDefinition(){
        return this.metricDefinition;
    }

    public void setMetricDefinition(MetricDefinition md){
        this.metricDefinition = md;
    }

}
