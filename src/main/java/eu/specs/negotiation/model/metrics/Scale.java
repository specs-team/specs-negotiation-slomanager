package eu.specs.negotiation.model.metrics;



import javax.xml.bind.annotation.*;

/**
 * Created by adrian on 3/31/15.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "scale", propOrder = {
        "quan", "qual"
})
@XmlRootElement
public class Scale {

    @XmlElement(name = "Quantitative")
    protected Qualitative quan;

    @XmlElement(name = "Qualitative")
    private Qualitative qual;

    public Qualitative getQuan() {
        return quan;
    }

    public void setQuan(Qualitative quan) {
        this.quan = quan;
    }

    public Qualitative getQual() {
        return qual;
    }

    public void setQual(Qualitative qual) {
        this.qual = qual;
    }
}
