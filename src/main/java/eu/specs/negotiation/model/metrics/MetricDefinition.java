package eu.specs.negotiation.model.metrics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by adrian on 3/31/15.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "unit","scale", "expression", "definition", "note"
})
public class MetricDefinition {

    @XmlElement(name = "unit", required = true)
    protected String unit;

    @XmlElement(name = "scale", required = true)
    protected Scale scale;

    @XmlElement(name = "expression", required = true)
    protected String expression;

    @XmlElement(name = "definition", required = true)
    protected String definition;

    @XmlElement(name = "note", required = true)
    protected String note;

    public String getUnit(){
        return this.unit;
    }

    public String getExpression(){
        return this.expression;
    }

    public String getDefinition(){
        return this.definition;
    }

    public String getNote(){
        return this.note;
    }

    public Scale getScale(){
        return this.scale;
    }

    public void setUnit(String unit){
        this.unit = unit;
    }

    public void setScale(Scale s){
        this.scale = s;
    }

    public void setExpression(String ex){
        this.expression = ex;
    }

    public void setDefinition(String def){
        this.definition = def;
    }

    public void setNote(String note){
        this.note = note;
    }
}
