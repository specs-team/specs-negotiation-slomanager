
package eu.specs.negotiation.model.service;

import eu.specs.negotiation.model.capability.Capability;
import eu.specs.negotiation.model.metrics.AbstractMetric;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="service_resources" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="resources_provider" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="VM" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;attribute name="appliance" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="hardware" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                           &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="zone" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="capabilities">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="capability" type="{http://specs-project.eu/schemas/sla_related}capabilityType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="security_metrics" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ConcreteMetric" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "serviceResources",
    "capabilities",
    "securityMetrics"
})
@XmlRootElement(name = "serviceDescription")
public class ServiceDescription {

    @XmlElement(name = "service_resources")
    protected List<ServiceResource> serviceResources;

    @XmlElementWrapper(name="capabilities")
    @XmlElement(name = "capability")
    protected List<Capability> capabilities;

    @XmlElementWrapper(name="security_metrics")
    @XmlElement(name = "ConcreteMetric", required = true)
    protected List<AbstractMetric> securityMetrics;

    /**
     * Gets the value of the serviceResources property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceResources property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceResources().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceResource }
     * 
     * 
     */
    public List<ServiceResource> getServiceResources() {
        if (serviceResources == null) {
            serviceResources = new ArrayList<ServiceResource>();
        }
        return this.serviceResources;
    }

    /**
     * Gets the value of the capabilities property.
     * 
     * @return
     *     possible object is List of
     *     {@link eu.specs.negotiation.model.capability.Capability }
     *     
     */
    public List<Capability> getCapabilities() {
        if(capabilities == null)
            capabilities = new ArrayList<Capability>();
        return capabilities;
    }

    /**
     * Sets the value of the capabilities property.
     *
     * @param value
     *     allowed object is
     *     {@link eu.specs.negotiation.model.capability.Capability }
     *     
     */
    public void setCapabilities(List<Capability> value) {
        this.capabilities = value;
    }

    /**
     * Gets the value of the securityMetrics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the securityMetrics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSecurityMetrics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link eu.specs.negotiation.model.metrics.AbstractMetric }
     * 
     * 
     */
    public List<AbstractMetric> getSecurityMetrics() {
        if (securityMetrics == null) {
            securityMetrics = new ArrayList<AbstractMetric>();
        }
        return this.securityMetrics;
    }

    public Capability getCapabilityById(String id){
        for(Capability c : this.capabilities){
            if(c.getId().equals(id))
                return c;
        }
        return null;
    }






    

}
