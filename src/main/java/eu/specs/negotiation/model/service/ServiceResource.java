package eu.specs.negotiation.model.service;

/**
 * Created by adrian on 3/31/15.
 */

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resources_provider" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VM" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="appliance" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="hardware" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="zone" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "resourcesProvider"
})
public class ServiceResource {

    @XmlElement(name = "resources_provider", required = true)
    protected List<ServiceResource.ResourcesProvider> resourcesProvider;

    /**
     * Gets the value of the resourcesProvider property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourcesProvider property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourcesProvider().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceResource.ResourcesProvider }
     *
     *
     */
    public List<ServiceResource.ResourcesProvider> getResourcesProvider() {
        if (resourcesProvider == null) {
            resourcesProvider = new ArrayList<ServiceResource.ResourcesProvider>();
        }
        return this.resourcesProvider;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VM" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="appliance" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="hardware" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="zone" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "vm"
    })
    public static class ResourcesProvider {

        @XmlElement(name = "VM")
        protected List<ServiceResource.ResourcesProvider.VM> vm;
        @XmlAttribute(name = "id", required = true)
        protected String id;
        @XmlAttribute(name = "name", required = true)
        protected String name;
        @XmlAttribute(name = "zone")
        protected String zone;
        @XmlAttribute(name = "descr")
        protected String descr;

        /**
         * Gets the value of the vm property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the vm property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getVM().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ServiceResource.ResourcesProvider.VM }
         *
         *
         */
        public List<ServiceResource.ResourcesProvider.VM> getVM() {
            if (vm == null) {
                vm = new ArrayList<ServiceResource.ResourcesProvider.VM>();
            }
            return this.vm;
        }

        /**
         * Gets the value of the id property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setId(String value) {
            this.id = value;
        }

        /**
         * Gets the value of the name property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the zone property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getZone() {
            return zone;
        }

        /**
         * Sets the value of the zone property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setZone(String value) {
            this.zone = value;
        }

        /**
         * Gets the value of the descr property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDescr() {
            return descr;
        }

        /**
         * Sets the value of the descr property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDescr(String value) {
            this.descr = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="appliance" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="hardware" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="descr" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class VM {

            @XmlAttribute(name = "appliance")
            protected String appliance;
            @XmlAttribute(name = "hardware")
            protected String hardware;
            @XmlAttribute(name = "descr")
            protected String descr;

            /**
             * Gets the value of the appliance property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getAppliance() {
                return appliance;
            }

            /**
             * Sets the value of the appliance property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setAppliance(String value) {
                this.appliance = value;
            }

            /**
             * Gets the value of the hardware property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getHardware() {
                return hardware;
            }

            /**
             * Sets the value of the hardware property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setHardware(String value) {
                this.hardware = value;
            }

            /**
             * Gets the value of the descr property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getDescr() {
                return descr;
            }

            /**
             * Sets the value of the descr property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setDescr(String value) {
                this.descr = value;
            }

        }

    }

}
