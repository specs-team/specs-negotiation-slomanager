package eu.specs.negotiation.model;

import eu.specs.negotiation.communication.SLAPlatformClient;
import eu.specs.negotiation.model.capability.Capability;
import eu.specs.negotiation.model.capability.SecurityControl;
import eu.specs.negotiation.model.service.ServiceDescription;
import eu.specs.negotiation.model.slo.ObjectiveList;
import eu.specs.negotiation.model.slo.SLO;
import eu.specs.negotiation.model.slo.SLOControlMapper;
import eu.specs.negotiation.model.wsag.Context;
import eu.specs.negotiation.model.wsag.offer.AgreementOffer;
import eu.specs.negotiation.model.wsag.slo.CustomServiceLevel;
import eu.specs.negotiation.model.wsag.slo.ServiceLevelObjective;
import eu.specs.negotiation.model.wsag.slo.ServiceProperty;
import eu.specs.negotiation.model.wsag.slo.Variable;
import eu.specs.negotiation.model.wsag.terms.GuaranteeTerm;
import eu.specs.negotiation.model.wsag.terms.ServiceDescriptionTerm;
import eu.specs.negotiation.model.wsag.terms.Term;
import eu.specs.negotiation.model.wsag.terms.Terms;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by adispataru on 4/24/15.
 */
@Path("/offers")
public class SLAAPI  {


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public List<AgreementOffer> getOffers(SLAInput slaInput){
        List<AgreementOffer> offers = new ArrayList<AgreementOffer>();

        AgreementOffer o = new AgreementOffer();
        o.setName("Y2-SPECS-APP");

        Context context = new Context();
        context.setAgreementInitiator("$SPECS-CUSTOMER");
        context.setAgreementResponder("$SPECS-APPLICATION");
        context.setExpirationTime(new Date(System.currentTimeMillis() + 60000));
        context.setServiceProvider("AgreementResponder");
        context.setTemplateName("Y2-APP-TEMPLATE");

        o.setContext(context);


        //TODO Hardcoded capabilities. These shall be inferred from the std, based on it's id.
        List<String> caps = new ArrayList<String>();
        caps.add("TLS");
        caps.add("WebPool");
        caps.add("E2EE");
        ServiceDescription sd = SLAPlatformClient.getStaticServiceDescription(caps);
        ServiceDescriptionTerm sdt = new ServiceDescriptionTerm();
        sdt.setName("Delivery");
        sdt.setServiceName("SecureWebServer");
        sdt.setServiceDescription(sd);

        GuaranteeTerm.ServiceScope scope = new GuaranteeTerm.ServiceScope();
        scope.setServiceName(sdt.getServiceName());

        List<Term> termList = new ArrayList<Term>();


        for(Capability c : sd.getCapabilities()){
            ServiceProperty serviceProperty = new ServiceProperty();
            serviceProperty.setName("//specs:capability[@id=\'" + c.getName() + "\']");
            serviceProperty.setServiceName(sdt.getServiceName());
            Set<Variable> vSet = new HashSet<Variable>();

            GuaranteeTerm guaranteeTerm = new GuaranteeTerm();
            guaranteeTerm.setServiceScope(scope);
            guaranteeTerm.setQualifyingCondition(true);

            ServiceLevelObjective slobjective = new ServiceLevelObjective();
            CustomServiceLevel customServiceLevel = new CustomServiceLevel();
            ObjectiveList objectiveList = new ObjectiveList();



            SLOControlMapper mapper = new SLOControlMapper();

            for(SLO slo : slaInput.slos){
                SecurityControl sc = new SecurityControl();
                sc.setId(mapper.getSecurityControlId(slo.getName()));
                if(c.getControlFramework().get(0).getSecurityControl().contains(sc)){
                    Variable v = new Variable();
                    v.setName(slo.getName());
                    v.setMetric(mapper.getMetricId(slo.getName()));
                    v.setLocation("//specs:security_control[@id=\'" +
                            mapper.getSecurityControlId(slo.getName()) + "\']");
                    vSet.add(v);


                    objectiveList.getSLO().add(slo);

                }

            }
            customServiceLevel.setObjectiveList(objectiveList);
            slobjective.setCustomServiceLevel(customServiceLevel);
            guaranteeTerm.setServiceLevelObjective(slobjective);
            serviceProperty.setVariableSet(vSet);
            termList.add(guaranteeTerm);
            termList.add(serviceProperty);


        }
        Terms terms = new Terms();
        termList.add(sdt);
        terms.setAll(termList);
        o.setTerms(terms);
        offers.add(o);


        return offers;
    }

}
