package eu.specs.negotiation.model.wsag.offer;

import eu.specs.negotiation.model.wsag.Context;
import eu.specs.negotiation.model.wsag.slo.ServiceProperty;
import eu.specs.negotiation.model.wsag.terms.Terms;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name", "context", "terms"
})
@XmlRootElement(name = "wsag:AgreementOffer")
public class AgreementOffer {

    @XmlElement(name = "wsag:Name")
    private String name;

    @XmlElement(name = "wsag:Context")
    private Context context;

    @XmlElement(name = "wsag:Terms")
    private Terms terms;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Terms getTerms() {
        return terms;
    }

    public void setTerms(Terms terms) {
        this.terms = terms;
    }
}
