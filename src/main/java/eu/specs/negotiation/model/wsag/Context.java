package eu.specs.negotiation.model.wsag;

import javax.xml.bind.annotation.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "agreementInitiator", "agreementResponder",
        "serviceProvider", "expirationTime", "templateName"
})
@XmlRootElement(name = "wsag:Context")
public class Context {
    @XmlElement(name = "wsag:AgreementInitiator")
    private String agreementInitiator;
    @XmlElement(name = "wsag:AgreementResponder")
    private String agreementResponder;
    @XmlElement(name = "wsag:ServiceProvider")
    private String serviceProvider;
    @XmlElement(name = "wsag:ExpirationTime")
    private Date expirationTime;
    @XmlElement(name = "wsag:TemplateName")
    private String templateName;

    public String getAgreementInitiator() {
        return agreementInitiator;
    }

    public void setAgreementInitiator(String agreementInitiator) {
        this.agreementInitiator = agreementInitiator;
    }

    public String getAgreementResponder() {
        return agreementResponder;
    }

    public void setAgreementResponder(String agreementResponder) {
        this.agreementResponder = agreementResponder;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
}
