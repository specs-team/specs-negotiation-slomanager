package eu.specs.negotiation.model.wsag.terms;

import eu.specs.negotiation.model.wsag.terms.Term;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "all",
})
@XmlRootElement(name = "Terms")
public class Terms {

    private List<Term> all;

    public List<Term> getAll() {
        if(all == null)
            all = new ArrayList<Term>();
        return all;
    }

    public void setAll(List<Term> all) {
        this.all = all;
    }
}
