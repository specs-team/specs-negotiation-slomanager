package eu.specs.negotiation.model.wsag.slo;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "customServiceLevel",
})
@XmlRootElement(name = "wsag:ServiceLevelObjective")
public class ServiceLevelObjective {

        @XmlElement(name = "wsag:CustomServiceLevel")
        private CustomServiceLevel customServiceLevel;

        public CustomServiceLevel getCustomServiceLevel() {
                return customServiceLevel;
        }

        public void setCustomServiceLevel(CustomServiceLevel customServiceLevel) {
                this.customServiceLevel = customServiceLevel;
        }
}
