package eu.specs.negotiation.model.wsag.slo;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name", "metric"
})
@XmlRootElement(name = "wsag:Variable")
public class Variable {


    @XmlAttribute(name = "wsag:Name")
    private String name;
    @XmlAttribute(name = "wsag:Metric")
    private String metric;
    @XmlElement(name = "wsag:Location")
    private String location;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

