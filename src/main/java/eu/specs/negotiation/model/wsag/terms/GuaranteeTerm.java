package eu.specs.negotiation.model.wsag.terms;

import eu.specs.negotiation.model.wsag.slo.ServiceLevelObjective;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "serviceScope", "qualifyingCondition",
        "serviceLevelObjective"
})
@XmlRootElement(name = "wsag:GuaranteeTerm")
public class GuaranteeTerm extends Term{

    @XmlElement(name = "ServiceScope")
    private ServiceScope serviceScope;
    @XmlElement(name = "QualifyingCondition")
    private boolean qualifyingCondition;
    @XmlElement
    private ServiceLevelObjective serviceLevelObjective;


    public boolean isQualifyingCondition() {
        return qualifyingCondition;
    }

    public void setQualifyingCondition(boolean qualifyingCondition) {
        this.qualifyingCondition = qualifyingCondition;
    }

    public ServiceScope getServiceScope() {
        return serviceScope;
    }

    public void setServiceScope(ServiceScope serviceScope) {
        this.serviceScope = serviceScope;
    }

    public ServiceLevelObjective getServiceLevelObjective() {
        return serviceLevelObjective;
    }

    public void setServiceLevelObjective(ServiceLevelObjective serviceLevelObjective) {
        this.serviceLevelObjective = serviceLevelObjective;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "serviceName",
    })
    @XmlRootElement(name = "wsag:ServiceScope")
    public static class ServiceScope {
        @XmlAttribute(name = "wsag:ServiceName")
        private String serviceName;

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }
    }



}
