package eu.specs.negotiation.model.wsag.slo;

import eu.specs.negotiation.model.slo.ObjectiveList;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "objectiveList",
})
@XmlRootElement(name = "wsag:CustomServiceLevel")
public class CustomServiceLevel {

        @XmlElement(name = "objectiveList")
        private ObjectiveList objectiveList;

        public ObjectiveList getObjectiveList() {
                return objectiveList;
        }

        public void setObjectiveList(ObjectiveList objectiveList) {
                this.objectiveList = objectiveList;
        }
}
