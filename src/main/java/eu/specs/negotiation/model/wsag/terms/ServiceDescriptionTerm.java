package eu.specs.negotiation.model.wsag.terms;

import eu.specs.negotiation.model.service.ServiceDescription;

import javax.xml.bind.annotation.*;

/**
 * Created by adispataru on 4/24/15.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name", "serviceName",
        "serviceDescription",
})
@XmlRootElement(name = "ServiceDescriptionTerm")
public class ServiceDescriptionTerm extends Term{
    @XmlAttribute(name = "wsag:Name")
    private String name;
    @XmlAttribute(name = "wsag:ServiceName")
    private String serviceName;
    @XmlElement(name = "specs:serviceDescription")
    private ServiceDescription serviceDescription;

    public ServiceDescription getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(ServiceDescription serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
