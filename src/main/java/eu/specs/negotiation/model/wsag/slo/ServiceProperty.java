package eu.specs.negotiation.model.wsag.slo;

import eu.specs.negotiation.model.wsag.terms.Term;

import javax.xml.bind.annotation.*;
import java.util.Set;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name", "serviceName", "variableSet"
})
@XmlRootElement(name = "wsag:ServiceProperty")
public class ServiceProperty extends Term {

    @XmlAttribute(name = "wsag:Name")
    private String name;
    @XmlAttribute(name = "Wsag:ServiceName")
    private String serviceName;
    @XmlElement(name = "wsag:VariableSet")
    private Set<Variable> variableSet;


    public Set<Variable> getVariableSet() {
        return variableSet;
    }

    public void setVariableSet(Set<Variable> variableSet) {
        this.variableSet = variableSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
