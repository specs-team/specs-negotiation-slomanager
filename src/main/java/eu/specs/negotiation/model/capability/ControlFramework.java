package eu.specs.negotiation.model.capability;

/**
 * Created by adrian on 3/31/15.
 */

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="security_control" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="control_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="importance_weight" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;anyAttribute/>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="frameworkName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "securityControl"
})
public class ControlFramework {

    @XmlElement(name = "security_control", required = true)
    protected List<SecurityControl> securityControl;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "frameworkName", required = true)
    protected String frameworkName;

    /**
     * Gets the value of the securityControl property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the securityControl property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSecurityControl().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityControl }
     *
     *
     */
    public List<SecurityControl> getSecurityControl() {
        if (securityControl == null) {
            securityControl = new ArrayList<SecurityControl>();
        }
        return this.securityControl;
    }

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the frameworkName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFrameworkName() {
        return frameworkName;
    }

    /**
     * Sets the value of the frameworkName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFrameworkName(String value) {
        this.frameworkName = value;
    }
}
