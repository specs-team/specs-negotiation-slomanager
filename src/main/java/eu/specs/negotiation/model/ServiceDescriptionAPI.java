package eu.specs.negotiation.model;

import eu.specs.negotiation.communication.SLAPlatformClient;
import eu.specs.negotiation.model.service.ServiceDescription;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by adrian on 3/27/15.
 */

@Path("/sdt")
public class ServiceDescriptionAPI {

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public ServiceDescription getServiceDescription(List<String> names){


        for(String s : names)
            System.out.println(s);
        ServiceDescription sdt = SLAPlatformClient.getStaticServiceDescription(names);

        return sdt;

    }

}
