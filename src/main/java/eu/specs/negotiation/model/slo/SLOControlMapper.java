package eu.specs.negotiation.model.slo;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adispataru on 4/25/15.
 */
public class SLOControlMapper {
    private Map<String, String> controlMap;
    private Map<String, String> metricMap;

    public SLOControlMapper(){

        //TODO This is HardCoded. Manage this through db
        controlMap = new HashMap<String, String>();
        controlMap.put("specs_tls_M4", "TLS_EKM-03");
        controlMap.put("specs_e2ee_M15", "E2EE_EKM-01");
        controlMap.put("specs_diversity_M1", "DIVERSITY_BCR-01");
        metricMap = new HashMap<String, String>();
        metricMap.put("specs_tls_M4", "M4");
        metricMap.put("specs_e2ee_M15", "M15");
        metricMap.put("specs_diversity_M1", "M1");

    }

    public String getSecurityControlId(String sloName){
        return controlMap.get(sloName);
    }

    public String getMetricId(String sloName){
        return metricMap.get(sloName);
    }

}
