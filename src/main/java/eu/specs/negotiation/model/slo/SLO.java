package eu.specs.negotiation.model.slo;

import javax.xml.bind.annotation.*;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="expression">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="op" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="importance_weight" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "expression",
        "importanceWeight"
})
public class SLO {

    @XmlElement(required = true)
    protected Expression expression;
    @XmlElement(name = "importance_weight", required = true)
    protected String importanceWeight;
    @XmlAttribute(name = "name")
    protected String name;

    /**
     * Gets the value of the expression property.
     *
     * @return
     *     possible object is
     *     {@link Expression }
     *
     */
    public Expression getExpression() {
        return expression;
    }

    /**
     * Sets the value of the expression property.
     *
     * @param value
     *     allowed object is
     *     {@link Expression }
     *
     */
    public void setExpression(Expression value) {
        this.expression = value;
    }

    /**
     * Gets the value of the importanceWeight property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getImportanceWeight() {
        return importanceWeight;
    }

    /**
     * Sets the value of the importanceWeight property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setImportanceWeight(String value) {
        this.importanceWeight = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(String value) {
        this.name = value;
    }


}
