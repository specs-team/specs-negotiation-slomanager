package eu.specs.negotiation.model;

import eu.specs.negotiation.model.slo.SLO;

import java.util.List;

/**
 * Created by adispataru on 4/24/15.
 */
public class SLAInput {
    public String stdId;
    public List<SLO> slos;
}
