package eu.specs.negotiation.communication;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import eu.specs.negotiation.model.capability.Capability;
import eu.specs.negotiation.model.capability.ControlFramework;
import eu.specs.negotiation.model.capability.SecurityControl;
import eu.specs.negotiation.model.metrics.*;
import eu.specs.negotiation.model.service.ServiceDescription;

import java.util.List;

/**
 * Created by adrian on 3/30/15.
 */
public class SLAPlatformClient {

    private static final String HOST = "";
    private static final String METRICS_PATH = "/rest/metrics/";

    public static void getMetrics(String capability){


        try {

            Client client = Client.create();

            WebResource webResource = client
                    .resource(HOST + METRICS_PATH + capability);

            ClientResponse response = webResource.accept("application/json")
                    .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            String output = response.getEntity(String.class);

            System.out.println("Output from Server .... \n");
            System.out.println(output);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public static ServiceDescription getStaticServiceDescription(List<String> names){
        String[][] capNames = {{"WebPool", "E2EE", "TLS"}, {"Diversity", "E2EE", "TLS"}};
        String[][] metrics = {{"M1", "M2"}, {"M15"}, {"M3", "M4", "M5", "M6", "M7", "M10", "M15"}};
        String[][] controls = {{"DIVERSITY_BCR-01"}, {"E2EE_EKM-01"}, {"EKM-01", "TLS_EKM-03", "TLS_IAM-02", "TLS_IAM-09", "TLS_AAC-03",}};

        ServiceDescription sd = new ServiceDescription();

        for(int i = 0; i < 3; i++){
            if(!names.contains(capNames[0][i]))
                continue;
            ControlFramework cf = new ControlFramework();
            cf.setId("CCM");
            cf.setFrameworkName("CCM 3.0");
            for(int j = 0; j < controls[i].length; j++){
                SecurityControl sc = new SecurityControl();
                sc.setId(controls[i][j]);
                if(j == 1) {
                    sc.setImportanceWeight("MEDIUM");
                    sc.setControlDescription(" ");
                }
                cf.getSecurityControl().add(sc);
            }

            Capability cap = new Capability();
            cap.setName(capNames[0][i]);
            cap.setId(capNames[1][i]);
            cap.getControlFramework().add(cf);
            sd.getCapabilities().add(cap);

            for(int j = 0; j < metrics[i].length; j++){
                AbstractMetric am = new ConcreteMetric();
                am.setName(metrics[i][j]);

                MetricDefinition md = new MetricDefinition();
                if(j == 1) {
                    md.setUnit("level");
                    Scale scale = new Scale();
                    scale.setQual(Qualitative.ORDINAL);
                    md.setScale(scale);
                }
                am.setMetricDefinition(md);

                sd.getSecurityMetrics().add(am);

            }


        }


        return sd;


    }

}
